FROM gitpod/workspace-full as tmp

USER root

ARG E3_GITLAB_PYPI_INDEX_URL
ARG HELM_VERSION=3.6.3
ARG HELMFILE_VERSION=0.140.0
ARG KUBECTL_VERSION=1.18.16
ARG TERRAFORM_VERSION=1.0.6

RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    curl \
    unzip \
    jq \
    libgeos-dev \
    liblapack-dev \
    libopenblas-dev \
    gfortran \
    python3-dev \
    python-all-dev \
    gcc \
    build-essential \
    libxslt-dev \
    libffi-dev \
    libssl-dev

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf aws awscliv2.zip

RUN curl -LO "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    rm -rf kubectl

RUN curl -sS "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx && \
    mv linux-amd64/helm /usr/bin/ && \
    rm -rf linux-amd64 && \
    curl -sSL -o /tmp/helmfile "https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64" && \
    chmod +x /tmp/helmfile && \
    mv /tmp/helmfile /usr/bin && \
    curl -sSL -o /tmp/sops "https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux" && \
    chmod +x /tmp/sops && \
    mv /tmp/sops /usr/bin

RUN curl -sS -o /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
    unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /tmp/terraform && \
    mv /tmp/terraform/terraform /usr/bin && \
    rm -rf /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip

COPY requirements.txt /tmp
COPY custom_requirements.txt /tmp

RUN pip install \
    --no-cache-dir \
    -r /tmp/requirements.txt && \
    pip install \
    --no-cache-dir \
    --index-url ${E3_GITLAB_PYPI_INDEX_URL} \
    --extra-index-url https://pypi.python.org/simple \
    -r /tmp/custom_requirements.txt && \
    rm -rf /tmp/custom_requirements.txt && rm -rf /tmp/custom_requirements.txt
    

RUN rm -rf requirements.txt custom_requirements.txt && \
    rm -rf /tmp/* && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

FROM tmp

USER gitpod
